This is a simple implementation of code splitting concept to show the very basic requirements, configuration and usage.
Of course with some research you can enhance it by adding some other features, etc. 
Feel free to customize the project and let me know if you have new ideas or any comments, thanks.