import axios from 'axios';
export function loader(url){
    axios.get(url).then(res=>{
        const {data} = res;
        import('./viewData').then(({view})=>{
            view(data);
        }).catch((err)=>{
            console.log(err)
        })
    })
}