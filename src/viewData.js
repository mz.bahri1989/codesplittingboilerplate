export function view(data) {
    let list = document.createElement('ol');
    data.map((item)=>{
        let li = document.createElement('li');
        const text = document.createTextNode(item.title);
        li.appendChild(text);
        list.appendChild(li);
    });
    document.getElementById('result').appendChild(list);
}