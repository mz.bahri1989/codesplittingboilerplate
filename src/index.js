document.addEventListener('click', () => {
    import('./loadData').then(({loader}) => {
        const todos = 'https://jsonplaceholder.typicode.com/todos',
            jibberJabber = 'https://jsonplaceholder.typicode.com/jibber'; //results in 404 which is enough for you to see that the viewData() will not run because the related file will not be called.
        loader(todos)
    })
}, false)