const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const src = './src';
module.exports = {
    mode: 'production',
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: false // set to true if you want JS source maps
            }),
        ]
    },
    entry: {
        //You only refer the main files here. Chuncks will be created from the import() lines inside the main files.
        index: src + '/index.js'

    },
    output: {
        filename: '[name].min.js',
        path: __dirname + '/assets/js',
        publicPath: './assets/js/'
    },
    plugins: [],
    resolve: {},
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/node_modules/, /\.css$/],
                use: {
                    loader: "babel-loader"
                }
            },
        ],
    }

};
